from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def test_spring_compose():
  driver = webdriver.Chrome()
  driver.get("http://localhost:8080")
  maintext = driver.find_element_by_xpath("/html/body/p[1]")
  print(maintext)
  driver.quit()

def test_spring_compose_against_grid():
  driver = webdriver.Remote(
    command_executor='http://127.0.0.1:4444/wd/hub',
    desired_capabilities={'browserName': 'chrome', 'javascriptEnabled': True}
  )
  try:
    driver.get("http://192.168.0.220:8080")
    maintext = driver.find_element_by_xpath("/html/body/p[1]")
    print(maintext)
  finally:
    driver.quit()


def test_other():
  driver = webdriver.Remote(
  command_executor='http://127.0.0.1:4444/wd/hub',
  desired_capabilities={'browserName': 'chrome', 'javascriptEnabled': True})
  driver.get("https://github.com")

  print(driver.title)
  assert "GitHub" in driver.title

  elem = driver.find_element_by_name("q")
  elem.send_keys("dzitkowskik")
  elem.send_keys(Keys.RETURN)
  assert "No results found." not in driver.page_source

  driver.quit()
